# frozen_string_literal: true

name 'cockroachdb-platform'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/cockroachdb-platform@'\
  'incoming.gitlab.com'
license 'Apache-2.0'
description 'Cookbook used to install and configure cockroachdb'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/cockroachdb-platform'
issues_url 'https://gitlab.com/chef-platform/cockroachdb-platform/issues'
version '1.6.0'

chef_version '>= 12.9'

depends 'ark'
depends 'cluster-search'
supports 'centos', '>= 7.1'
